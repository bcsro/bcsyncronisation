import excel "C:\Users\Slava\bcsyncronisation\Data2_new_info\All_annual_data.xlsx", sheet("Sheet1 (2)") firstrow

reshape long data, i(unit_id indicator_id) j(time)

drop indicator

reshape wide data, i(unit_id time) j(indicator_id)

move Country unit_id

drop unit_id

rename data1 ca_def

label variable ca_def "Current account deficit"

rename data2 cred2gdp

label variable cred2gdp "Private credit in GDP"

rename data3 gdpgrowth

label variable gdpgrowth "GDP growth"

rename data4 govdef

label variable govdef "Government deficit"

save "C:\Users\Slava\bcsyncronisation\Data2_new_info\stata\data_init_2.dta"

