mahascore ca_def cred2gdp gdpgrowth govdef, gen(distances) refmeans compute_invcovarmat unsquared
sort Country
sort Country time
twoway (line distances time), ytitle(Mahalanobis distance) xtitle(Time) by(Country)
