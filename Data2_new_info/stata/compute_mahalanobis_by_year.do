cd "C:\Users\Slava\bcsyncronisation\Data2_new_info\stata"


forval t=2005/2013 {
display(`t')
use "data_all_2005_on.dta", clear
sort time Country
keep if time==`t'
mahascore ca_def cred2gdp gdpgrowth govdef, gen(distances) refmeans compute_invcovarmat unsquared
local filename="mahala_"+string(`t')+".dta"
save `filename', replace
}

clear

forval t=2005/2013 {
local filename="mahala_"+string(`t')+".dta"
append using `filename'
}

sort Country time

twoway (line distances time), ytitle(Mahalanobis distance) xtitle(Time) by(Country)

graph save Graph "Graph.gph", replace
 
graph export "Graph.pdf", as(pdf) replace

encode Country, generate(Countryid) label(Country)

xtset Countryid time

xtline distances if inlist(Country,"Austria","Belgium","Denmark","Finland","France","Germany") | inlist(Country,"Luxembourg","Netherlands","Sweden","United Kingdom"), overlay legend(on)
graph save Graph "Graph_G1.gph", replace
graph export "Graph_G1.pdf", as(pdf) replace


xtline distances if inlist(Country,"Estonia","Latvia","Lithuania","Slovakia","Slovenia") , overlay legend(on)
graph save Graph "Graph_G2.gph", replace
graph export "Graph_G2.pdf", as(pdf) replace

xtline distances if inlist(Country,"Bulgaria","Croatia","Czech Republic","Hungary","Malta","Poland","Romania") , overlay legend(on)
graph save Graph "Graph_G3.gph", replace
graph export "Graph_G3.pdf", as(pdf) replace

xtline distances if inlist(Country,"Cyprus","Greece","Ireland","Italy","Portugal","Spain") , overlay legend(on)
graph save Graph "Graph_G4.gph", replace
graph export "Graph_G4.pdf", as(pdf) replace

save "All_mahalanobis_final.dta", replace
