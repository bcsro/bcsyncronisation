\begin{thebibliography}{}

\bibitem[Artis et~al., 2004]{Artis2004}
Artis, M., Marcellino, M., and Proietti, T. (2004).
\newblock {Characterising the Business Cycle for Accession Countries}.
\newblock Technical Report May, CEPR.

\bibitem[Artis et~al., 1997]{Artis1997a}
Artis, M.~J., Kontolemis, Z.~G., and Osborn, D.~R. (1997).
\newblock {Business Cycles for G7 and European Countries}.
\newblock {\em The Journal of Business}, 70(2):249--279.

\bibitem[Artis and Zhang, 1997]{Artis1997}
Artis, M.~J. and Zhang, W. (1997).
\newblock {International Business Cycles and the ERM : Is There a European
  Business Cycle ?}
\newblock {\em International Journal of Finance and Economics}, 2(July
  1995):1--16.

\bibitem[Artis and Zhang, 1999]{Artis1999}
Artis, M.~J. and Zhang, W. (1999).
\newblock {Further Evidence on the International Business Cycle and the ERM: Is
  There a European Business Cycle?}
\newblock {\em Oxford Economic Papers}, 51:120--32.

\bibitem[Bry and Boschan, 1971]{Bry1971}
Bry, G. and Boschan, C. (1971).
\newblock {\em {Cyclical Analysis of Time Series : Selected Procedures and
  Computer Programs}}.
\newblock National Bureau of Economic Research.

\bibitem[Burns and Mitchell, 1946]{burn46}
Burns, A.~F. and Mitchell, W.~C. (1946).
\newblock {\em {Measuring Business Cycles}}.
\newblock NBER Books. National Bureau of Economic Research, Inc.

\bibitem[Giannone et~al., 2009]{Giannone2009}
Giannone, D., Lenza, M., and Reichlin, L. (2009).
\newblock {Business cycles in the euro area}.
\newblock Technical Report 1010, European Central Bank.

\bibitem[Harding and Pagan, 1999]{Harding1999}
Harding, D. and Pagan, A. (1999).
\newblock {Knowing the Cycle}.
\newblock Melbourne Institute Working Paper Series {WP 1999N12}, Melbourne
  Institute of Applied Economic and Social Research, The University of
  Melbourne.

\bibitem[Harding and Pagan, 2002]{Harding2002}
Harding, D. and Pagan, A. (2002).
\newblock {Dissecting the cycle: A methodological investigation}.
\newblock {\em Journal of Monetary Economics}, 49:365--381.

\bibitem[Harding and Pagan, 2005]{Harding2005}
Harding, D. and Pagan, A. (2005).
\newblock {A suggested framework for classifying the modes of cycle research}.
\newblock {\em Journal of Applied Econometrics}, 20(May 2003):151--159.

\bibitem[Harding and Pagan, 2006]{Harding2006}
Harding, D. and Pagan, A. (2006).
\newblock {Synchronization of cycles}.
\newblock {\em Journal of Econometrics}, 132:59--79.

\bibitem[Krolzig and Toro, 2005]{Krolzig2001}
Krolzig, H.~M. and Toro, J. (2005).
\newblock {Classical and modern business cycle measurement: The European case}.
\newblock {\em Spanish Economic Review}, 7(60):1--21.

\end{thebibliography}
