clear all;
close all;
clc;

load PCA_estim;
load dating_oecd;
irisset('pdflatexpath','C:\MiKTeX_2.9\miktex\bin\x64\pdflatex.exe')
%% Make dating on PCA
countrylist=fieldnames(PCA_estim);
dating_PCA=struct;
for i=1:length(countrylist)
dating_PCA.(countrylist{i})=mbbq(PCA_estim.(countrylist{i}).score_level,'thresh',5,'complete',0);
end

rep=report.new('PCA analysis','title','PCA analysis');

for i=1:length(countrylist)
rep.section(countrylist{i});
dbplot(data_PCA_level.(countrylist{i}));
rep.userfigure('Raw data',gcf);
close(gcf);
dbplot(PCA_estim.(countrylist{i}),{'score_level','score_delta'});
rep.userfigure(['PCA analysis fist component explains ' num2str(PCA_estim.(countrylist{i}).explained_delta) '%'],gcf);
close(gcf);


rep.align('Dating results',2,2);
plotmbbq(dating_PCA.(countrylist{i}));
title(countrylist{i})
rep.userfigure('PCA',gcf);
close(gcf);

plotmbbq(dating.(countrylist{i}));
title(countrylist{i})
rep.userfigure('GDP',gcf);
close(gcf);

rep.align('Dating results peaks',2,2);
rep.table('Peaks PCA','range',dating_PCA.(countrylist{i}).peaks);
rep.series('',tseries);

rep.table('Peaks GDP','range',dating.(countrylist{i}).peaks);
rep.series('',tseries);

rep.align('Dating results throughs',2,2);

rep.table('Troughs PCA','range',dating_PCA.(countrylist{i}).troughs);
rep.series('',tseries);

rep.table('Troughs GDP','range',dating.(countrylist{i}).troughs);
rep.series('',tseries);

heatdb.(countrylist{i})=dating.(countrylist{i}).s;
heatdb.([countrylist{i} '_PCA'])=dating_PCA.(countrylist{i}).s;

% heatdbtemp=struct;
% heatdbtemp.(countrylist{i})=dating.(countrylist{i}).s;
% heatdbtemp.([countrylist{i} '_PCA'])=dating_PCA.(countrylist{i}).s;
% 
% heat1=db2array(heatdbtemp);
% 
% [lt,~]=size(heat1);
%  countriesheatdbtemp=fieldnames(heatdbtemp);
%  countriesheatdbtemp2=countriesheatdbtemp;
%  heat=nan(lt,length(countriesheatdbtemp2));
%     for j=1: length(countriesheatdbtemp)
%     countriesheatdbtemp2{j}=strrep(countriesheatdbtemp{j},'_',' ');
%     heat(:,j)=heat1(:,j);
%     end
% 
% ldb=length(dbrange(heatdbtemp));
% listtime(1:ldb)=cellstr('');
% timestr=dat2str(dbrange(heatdbtemp));
% listtime(1:12:ldb)=timestr(1:12:ldb);
% 
% Mcol=zeros(2,3);
% Mcol(2,:)=[0 0 1];
% Mcol(1,:)=[1 0 0];        
%         
% 
% hf=figure;
% 
% [hImage, hText, hTick] = heatmap(heat', listtime, countriesheatdbtemp2, [], 'NaNColor', [1 1 1],...
%     'Colormap',Mcol,'MinColorValue',0,'MaxColorValue',1,'Colorbar',1);
% 
% set(gca,'YTick',1:1:2);
% set(gca,'yTickLabel',countriesheatdbtemp2);
% set(gca,'XTick',1:12:ldb);
% set(gca,'XTickLabel',timestr(1:12:ldb));
% cbar_handle = findobj(hf,'tag','Colorbar');
% set(cbar_handle,'Yticklabel',{'Recession','','','','','','','','','','Expansion'});
% 
% %%
% rotateXLabels(gca,90);
% 
% saveas(hf,['heat_PCA_' countrylist{i} '.pdf']);



end

rep.publish('PCA_analysis.pdf','display',true,'echo=',true,'makeTitle=',true);

heat1=db2array(heatdb);

[lt,~]=size(heat1);
 countriesheatdb=fieldnames(heatdb);
 countriesheatdb2=countriesheatdb;
 heat=nan(lt,length(countriesheatdb2));
    for j=1: length(countriesheatdb)
    countriesheatdb2{j}=strrep(countriesheatdb{j},'_',' ');
    heat(:,j)=heat1(:,j);
    end

ldb=length(dbrange(heatdb));
listtime(1:ldb)=cellstr('');
timestr=dat2str(dbrange(heatdb));
listtime(1:8:ldb)=timestr(1:8:ldb);

Mcol=zeros(2,3);
Mcol(2,:)=[0 0 1];
Mcol(1,:)=[1 0 0];        
        

hf=figure;

[hImage, hText, hTick] = heatmap(heat', listtime, countriesheatdb2, [], 'NaNColor', [1 1 1],...
    'Colormap',Mcol,'MinColorValue',0,'MaxColorValue',1,'Colorbar',1);

set(gca,'YTick',1:1:length(countriesheatdb2));
set(gca,'yTickLabel',countriesheatdb2);
set(gca,'XTick',1:8:ldb);
set(gca,'XTickLabel',timestr(1:8:ldb));
grid on;
cbar_handle = findobj(hf,'tag','Colorbar');
set(cbar_handle,'Yticklabel',{'Recession','','','','','','','','','','Expansion'});

%%
rotateXLabels(gca,90);

saveas(hf,['heat_PCA_.pdf']);



