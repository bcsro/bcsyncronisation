clear all;
close all;
clc;
load heatmap_oecd.mat;


countries=fieldnames(heatdb);
countries(ismember(countries,'Lithuania'))=[];
lc=length(countries);

cbase={'USA','Germany'};
lcb=length(cbase);

rhots=struct;
% stdts=struct;

for i=1:lc

    for j=1:lcb
       if strcmp(countries{i},cbase{j})
           rhots.(cbase{j}).(countries{i})=1;
           continue
       end
       rhotemp=tseries;
%         stdtemp=tseries;
       
       for t=qq(2004,1):qq(2014,3)
       x1=heatdb.(countries{i});
       x2=heatdb.(cbase{j});
       start_x1=get(x1,'start');
       start_x2=get(x2,'start');
        x1=x1(max(start_x1,start_x2):t);
        x2=x2(max(start_x1,start_x2):t);

        if (any(x1-x2))
        temp=corrcoef(double([x1,x2]));
        rhotemp(t)=temp(1,2);
%  [rhotemp(t),stdtemp(t),~,~]=compute_rhomean(x1,x2);
        else
         rhotemp(t)=1;
%           stdtemp(t)=0;
        end
    
       end
       
       rhots.(cbase{j}).(countries{i})=rhotemp;
%         stdts.(cbase{j}).(countries{i})=stdtemp;
    end
    
end

% Make a report

myrep=report.new('Correlation coefficient in time');

for i=1:lc
    
myrep.figure(['Correlation coefficient for ' countries{i}]);
myrep.graph('','legend',true);
for j=1:lcb
myrep.series(cbase{j},rhots.(cbase{j}).(countries{i}));
end
end

myrep.publish(['Report7_Rho_ts'  strrep(strrep(datestr(now),':','_'),' ','_') '.pdf']);