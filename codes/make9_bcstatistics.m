clear all;
close all;
clc;

load dating_oecd;

countries={'Iceland','Ireland','UK','Switzerland','Germany','USA','Norway','Finland','Sweden','Denmark','Netherlands','France',...
                'Luxembourg','Austria','Belgium','Spain','Italy','Portugal','Greece',...
                'Latvia','Estonia','EU_28','Lithuania','Malta','Cyprus',...
                'Bulgaria','Romania','Czech_Republic','Hungary','Slovenia','Slovakia'};
lc=length(countries);

statistics=zeros(lc,10);

for i=1:lc
statistics(i,1:2)=dating.(countries{i}).duration;
statistics(i,3:4)=dating.(countries{i}).amplitude;
statistics(i,5:6)=dating.(countries{i}).steep;
statistics(i,7:8)=dating.(countries{i}).cumulative;
statistics(i,9:10)=dating.(countries{i}).excess;
end

name_col={'Duration R','Duration E','Amplitude R','Amplitude E',...
          'Slope R','Slope E','Gain R','Gain E',...
          'Excess R','Excess E',};

myrep=report.new('Business cycle statistics');

myrep.matrix('Statistics',statistics,'rowNames',countries,...
                                           'colNames',name_col,'format=','%.1f');

% Duration
hf=figure;
scatter(statistics(:,2),statistics(:,1));
title('Duration');
xlabel Expansion;
ylabel Contraction;
h = labelpoints (statistics(:,2),statistics(:,1), countries,'position','NE');                                       
myrep.userfigure('',hf);

% Amplitude
hf=figure;
scatter(statistics(:,4),statistics(:,3));
title('Amplitude');
xlabel Expansion;
ylabel Contraction;
h = labelpoints (statistics(:,4),statistics(:,3), countries,'position','NE');                                       
myrep.userfigure('',hf);

% Slope
hf=figure;
scatter(statistics(:,6),statistics(:,5));
title('Slope');
xlabel Expansion;
ylabel Contraction;
h = labelpoints (statistics(:,6),statistics(:,5), countries,'position','NE');                                       
myrep.userfigure('',hf);

% Gain
hf=figure;
scatter(statistics(:,8),statistics(:,7));
title('Gain');
xlabel Expansion;
ylabel Contraction;
h = labelpoints (statistics(:,8),statistics(:,7), countries,'position','NE');                                       
myrep.userfigure('',hf);

% Excess
hf=figure;
scatter(statistics(:,10),statistics(:,9));
title('Excess');
xlabel Expansion;
ylabel Contraction;
h = labelpoints (statistics(:,10),statistics(:,9), countries,'position','NE');                                       
myrep.userfigure('',hf);

myrep.publish(['Report9_BC_statistics'  strrep(strrep(datestr(now),':','_'),' ','_') '.pdf'],...
    'tempDir=','Rep9tmp','cleanup=',false);


