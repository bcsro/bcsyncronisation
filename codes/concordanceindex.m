function ci=concordanceindex(x1,x2)

range1=get(x1,'range');
range2=get(x2,'range');
range_start=max(range1(1),range2(1));
range_end=min(range1(end),range2(end));
x1=x1(range_start:range_end);
x2=x2(range_start:range_end);
T=length(range_start:range_end);

ci=(sum(x1.*x2)+sum((1-x1).*(1-x2)))/T;
end