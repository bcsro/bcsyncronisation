clear all;
clc;

myrep=report.new('Data comparison');

gdp_eurostat=load('gdp_Eurostat.mat');
gdp_oecd=load('gdp_oecd.mat');
% gdp_oecd2=load('gdp_oecd2.mat');
countries=fieldnames(gdp_oecd.gdp');
lc=length(countries);

for j=1:lc
       qoq_rate_eurostat.(countries{j})=(gdp_eurostat.gdp.(countries{j})-gdp_eurostat.gdp.(countries{j}){-1});
       qoq_rate_oecd.(countries{j})=(gdp_oecd.gdp.(countries{j})-gdp_oecd.gdp.(countries{j}){-1});
%        qoq_rate_oecd2.(countries{j})=(gdp_oecd2.gdp.(countries{j})-gdp_oecd2.gdp.(countries{j}){-1});
       diff_eurostat_oecd1.(countries{j})=qoq_rate_eurostat.(countries{j})-qoq_rate_oecd.(countries{j});
%        diff_eurostat_oecd2.(countries{j})=qoq_rate_eurostat.(countries{j})-qoq_rate_oecd2.(countries{j});
       
myrep.figure('');
myrep.graph(gdp_eurostat.gdp.(countries{j}).comment);
myrep.series('',diff_eurostat_oecd1.(countries{j}));

end;

myrep.publish(['GDP_data_comp'  strrep(strrep(datestr(now),':','_'),' ','_') '.pdf']);