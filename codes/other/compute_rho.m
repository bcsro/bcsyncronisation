function [rho,std,tstat,pv]=compute_rho(x1,x2)


range1=get(x1,'range');
range2=get(x2,'range');
range_start=max(range1(1),range2(1));
range_end=min(range1(end),range2(end));
x1=x1(range_start:range_end);
x2=x2(range_start:range_end);
x1=double(x1);
x2=double(x2);
% data=[x1,x2];

[TT,~]  = size([x1,x2]);
JJ = TT-2;

% estimate statistics of interest
temp     = cov([x1,x2]);
var_1    = temp(1,1);
var_2    = temp(2,2);
m1=mean(x1);
m2=mean(x2);
% x1=x1-m1;
% x2=x2-m2;
corr_12  = temp(2,1)/sqrt(var_1*var_2);
rho=corr_12;
ratio_12 = sqrt(var_1/var_2);

h_resid  = [(x2-m2).^2*ratio_12^2-(x1-m1).^2,(x2-m2).^2*ratio_12*corr_12-(x1-m1).*(x2-m2)]';

D = [2*var_2*ratio_12,0;var_2*corr_12,var_2*ratio_12];
           

 
% obtain estimate for the sigma_0 matrix (the variance-covariance matrix)
% using the Newey-West kernel

sigma_0_nw    = (h_resid*h_resid')/TT;

for j = 1:JJ
    temp = h_resid(:,j+1:end)*(h_resid(:,1:end-j)')/TT;
    sigma_0_nw = sigma_0_nw + (temp+temp')*(1-j/(JJ+1));
end
V_nw = inv((D/sigma_0_nw)*D');
SE_nw = sqrt(V_nw(2,2)/TT);
std=SE_nw;
tstat = rho/SE_nw;
pv=2*normcdf(-abs(tstat),0,1);
end