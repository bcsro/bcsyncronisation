function [var,corr,sigma_0_nw]=compute_statistics(x1,x2)

data=[x1,x2];

range1=get(x1,'range');
range2=get(x2,'range');
range_start=max(range1(1),range2(1));
range_end=min(range1(end),range2(end));
x1=x1(range_start:range_end);
x2=x2(range_start:range_end);
data=double([x1,x2]);

[TT, ~]  = size(data);
JJ         = 10;

% estimate statistics of interest
temp     = cov(data);
var_i    = temp(1,1);
var_s    = temp(2,2);
corr_is  = temp(2,1)/sqrt(var_i*var_s);
ratio_is = sqrt(var_i/var_s);

var(1)=var_i;
var(2)=var_s;
corr=corr_is;

% construct the residuals, i.e., h(x_t;theta_hat)

h_resid  = [data(:,2).^2*ratio_is^2-data(:,1).^2,data(:,2).^2*ratio_is*corr_is-data(:,1).*data(:,2)]';

% obtain estimate for the D matrix

D        = [2*var_s*ratio_is,0;var_s*corr_is,var_s*ratio_is];
           

 
% obtain estimate for the sigma_0 matrix (the variance-covariance matrix)
% using the truncated and the Newey-West kernel

sigma_0_tr    = (h_resid*h_resid')/TT;
sigma_0_nw    = (h_resid*h_resid')/TT;

for j = 1:JJ
    temp = h_resid(:,j+1:end)*(h_resid(:,1:end-j)')/TT;
    sigma_0_tr = sigma_0_tr + (temp+temp');
    sigma_0_nw = sigma_0_nw + (temp+temp')*(1-j/(JJ+1));
end
return