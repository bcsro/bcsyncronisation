function [tstat,pv]=compare_means(x1,x2)

range1=get(x1,'range');
range2=get(x2,'range');
range_start=max(range1(1),range2(1));
range_end=min(range1(end),range2(end));
x1=x1(range_start:range_end);
x2=x2(range_start:range_end);
x1=double(x1);
x2=double(x2);
x0=mean(x1)-mean(x2); 

T=length(x1);
J=T-2;
h_resid=[x1-x2-x0];

sigma_0_nw= (h_resid'*h_resid)/T;

for j = 1:J
    temp = (h_resid(j+1:end)')*h_resid(1:end-j)/T;
    sigma_0_nw = sigma_0_nw + 2*temp*(1-j/(J+1));
end

tstat = x0/sqrt(sigma_0_nw/T);
pv=2*normcdf(-abs(tstat),0,1);

end