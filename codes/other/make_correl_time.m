clear all;
close all;
clc;
load heatmap.mat;

countries={'CH_SA','UK_SA','Finland','Sweden','Norway','USA','France','Germany','EA_19','EU_28',...
    'Greece','Portugal','Italy','Spain',...
    'Hungary','Czech_Republic', ...
    'Romania','Bulgaria'};
lc=length(countries);

cbase={'USA','EA_19'};
lcb=length(cbase);

rhots=struct;
stdts=struct;

for i=1:lc

    for j=1:lcb
       if strcmp(countries{i},cbase{j})
           continue
       end
       rhotemp=tseries;
        stdtemp=tseries;
       
       for t=qq(2004,1):qq(2014,3)
       x1=heatdb.(countries{i});
       x2=heatdb.(cbase{j});
        x1=x1{t-36:t};
        x2=x2{t-36:t};

        if (any(x1-x2))
%        temp=corrcoef(double([x1,x2]));
 [rhotemp(t),stdtemp(t),~,~]=compute_rhomean(x1,x2);
        else
         rhotemp(t)=1;
          stdtemp(t)=0;
        end
    
       end
       
       rhots.(cbase{j}).(countries{i})=rhotemp;
        stdts.(cbase{j}).(countries{i})=stdtemp;
    end
    
end

% Make a report

