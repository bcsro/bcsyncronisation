function [rho,SE_nw,tstat,pv]=compute_rhomean(x1,x2)

tstat=0;
pv=0;
range1=get(x1,'range');
range2=get(x2,'range');
range_start=max(range1(1),range2(1));
range_end=min(range1(end),range2(end));
x1=x1(range_start:range_end);
x2=x2(range_start:range_end);
x1=double(x1);
x2=double(x2);

[TT,~]  = size([x1,x2]);
JJ =TT-2;%floor(10*TT^(1/3));


% estimate statistics of interest
temp     = cov([x1,x2],1);
mu1      = mean(x1);
mu2      = mean(x2);
var_1    = temp(1,1);
var_2    = temp(2,2);
S1       = sqrt(var_1);
S2       = sqrt(var_2);
rho      = temp(2,1)/sqrt(var_1*var_2);

h_resid  = [x1-mu1;x2-mu2;(x1-mu1).^2-S1^2;(x2-mu2).^2-S2^2;((x1-mu1)/S1).*((x2-mu2)/S2)-rho]';

D = [-1     0       0       0       0
      0    -1       0       0       0
      0     0       -2*S1   0       0
      0     0       0       -2*S2   0
      -rho  -rho   -rho/S1  -rho/S2   -1];
 
% obtain estimate for the sigma_0 matrix (the variance-covariance matrix)
% using the Newey-West kernel

sigma_0_nw    = (h_resid*h_resid')/TT;

for j = 1:JJ
    temp = h_resid(:,j+1:end)*(h_resid(:,1:end-j)')/TT;
    sigma_0_nw = sigma_0_nw + (temp+temp')*(1-j/(JJ+1));
end

V_nw = inv((D/sigma_0_nw)*D');
SE_nw = sqrt(V_nw(end,end)/TT);
tstat = rho/SE_nw;
pv=2*normcdf(-abs(tstat),0,1);
end