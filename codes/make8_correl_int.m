clear all;
close all;
clc

% load dating
load heatmap_oecd.mat;

periods_end=[qq(1989,4),qq(2007,4),qq(2014,3)];

periods_end=[qq(2003,4),qq(2014,3)];

countries=fieldnames(heatdb);
lc=length(countries);
lp=length(periods_end);
rho_var=nan(lp,lc,lc);

heatdb_range=dbrange(heatdb);

heatdb1=dbclip(heatdb,heatdb_range(1):periods_end(1));
heatdb_array=db2array(heatdb1);
rho_var(1,:,:)=corr(heatdb_array,'rows','pairwise');

for i=2:lp
heatdb1=dbclip(heatdb,periods_end(i-1)+1:periods_end(i));
heatdb_array=db2array(heatdb1);
rho_var(i,:,:)=corr(heatdb_array,'rows','pairwise');
end

myrep=report.new('Variable correlation');

myrep.matrix(strcat('Rho for period:',dat2str(heatdb_range(1)),'-',dat2str(periods_end(1))), squeeze(rho_var(1,:,:)),'rowNames',countries,...
                'colNames',countries);   

for i=2:lp

myrep.matrix(strcat('Rho for period:',dat2str(periods_end(i-1)+1),'-',dat2str(periods_end(i))), squeeze(rho_var(i,:,:)),'rowNames',countries,...
                'colNames',countries);    
end       

posG=find(ismember(countries,'Germany'));
posUS=find(ismember(countries,'USA'));

rhoall=nan(lc,lp*2);

for i=1:lp
rhoall(:,i)=squeeze(rho_var(i,:,posG));
rhoall(:,i+lp)=squeeze(rho_var(i,:,posUS));
end

myrep.matrix('Rho agregated', rhoall,'rowNames',countries,...
                'colNames',{['Germany:',char(dat2str(heatdb_range(1))),'-',char(dat2str(periods_end(1)))],...
                            ['Germany:',char(dat2str(periods_end(1)+1)),'-',char(dat2str(periods_end(2)))],...
%                             ['Germany:',char(dat2str(periods_end(2)+1)),'-',char(dat2str(periods_end(3)))],...
                            ['USA:',char(dat2str(heatdb_range(1))),'-',char(dat2str(periods_end(1)))],...
                            ['USA:',char(dat2str(periods_end(1)+1)),'-',char(dat2str(periods_end(2)))]});%,...
%                             ['USA:',char(dat2str(periods_end(2)+1)),'-',char(dat2str(periods_end(3)))]});

myrep.publish(['Report8_rho_var_'  strrep(strrep(datestr(now),':','_'),' ','_') '.pdf']);