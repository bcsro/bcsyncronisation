clear all;
close all;
clc

% load dating
load dating_oecd

 countries=fieldnames(dating);
% countries={'CH_SA','UK_SA','Finland','Sweden','Norway','USA','France','Germany','EA_19','EU_28',...
%     'Greece','Portugal','Italy','Spain',...
%     'Hungary','Czech_Republic', ...
%     'Romania','Bulgaria'};

% countries={'Germany','EU_28','Czech_Republic','Greece','Spain','France',...
%     'Italy','Hungary','Portugal','Finland','Sweden','EA_19','Norway','USA'}; % la OECD avem EA 18 in loc de EA 19

lc=length(countries);
heatdb=struct;

myrep=report.new('Dating');

for i=1:lc

% myrep.align('',2,1);
hf=figure;
plotmbbq(dating.(countries{i}));
title(countries{i})
myrep.userfigure('',hf);

% myrep.align('',3,3);
x=zeros(8,2);
x(1,:)=dating.(countries{i}).duration;
x(2,:)=dating.(countries{i}).amplitude;
x(3,:)=dating.(countries{i}).cumulative;
x(4,:)=dating.(countries{i}).excess;
x(5,:)=dating.(countries{i}).cv_dur;
x(6,:)=dating.(countries{i}).cv_amp;
x(7,:)=dating.(countries{i}).cv_xss;
x(8,:)=dating.(countries{i}).steep;


myrep.matrix('Statistics',x,'rowNames',{'duration','amplitude','cumulative','excess',...
                                           'cv_dur','cv_amp','cv_xss','steep'},...
                                           'colNames',{'R','E'});
myrep.table('Peaks','range',dating.(countries{i}).peaks);
myrep.series('',tseries);

myrep.table('Troughs','range',dating.(countries{i}).troughs);
myrep.series('',tseries);
myrep.pagebreak;
heatdb.(countries{i})=dating.(countries{i}).s;
end

myrep.publish(['Report4_Dating_oecd'  strrep(strrep(datestr(now),':','_'),' ','_') '.pdf']);
save heatmap_oecd.mat heatdb;
 close all;
return;
%%

heat1=db2array(heatdb);

[lt,~]=size(heat1);

countries_heat={'Iceland','Ireland','UK','Switzerland','Germany','USA','Norway','Finland','Sweden','Denmark','Netherlands','France',...
                'Luxembourg','Austria','Belgium','Spain','Italy','Portugal','Greece',...
                'Latvia','Estonia','EU_28','Lithuania','Malta','Cyprus',...
                'Bulgaria','Romania','Czech_Republic','Hungary','Slovenia','Slovakia'};
for i=1: length(countries_heat)
countries_heat2{i}=strrep(countries_heat{i},'_',' ');
end
heat=nan(lt,length(countries_heat));
for i=1:length(countries_heat)
pos=find(ismember(countries,countries_heat{i}));    
heat(:,i)=heat1(:,pos);
end

ldb=length(dbrange(heatdb));
listtime(1:ldb)=cellstr('');
timestr=dat2str(dbrange(heatdb));
listtime(1:12:ldb)=timestr(1:12:ldb);
% heatT=array2table(db2array(heatdb),'RowNames',timestr,'VariableNames',countries_heat2);
% writetable(heatT,'heat_big_oecd.csv','WriteRowNames',true);
Mcol=zeros(2,3);
Mcol(2,:)=[0 0 1];
Mcol(1,:)=[1 0 0];        
        

hf=figure;
% plot(HMobj,hf);
[hImage, hText, hTick] = heatmap(heat', listtime, countries_heat2, [], 'NaNColor', [1 1 1],...
    'Colormap',Mcol,'MinColorValue',0,'MaxColorValue',1,'Colorbar',1);

%%
set(gca,'YTick',1:1:lc);
set(gca,'yTickLabel',countries_heat2);
set(gca,'XTick',1:12:ldb);
set(gca,'XTickLabel',timestr(1:12:ldb));
cbar_handle = findobj(hf,'tag','Colorbar');
set(cbar_handle,'Yticklabel',{'Recession','','','','','','','','','','Expansion'});

%%
rotateXLabels(gca,90);

%%
% myrep.userfigure('Extended Heatmap',hf,'figurescale',0.8);
saveas(hf,'heat_map_ext_oecd.pdf');








