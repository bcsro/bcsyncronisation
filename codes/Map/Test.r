
library(rworldmap)
library(RColorBrewer)
#library(classInt)


d = read.csv("rhofullsample.txt")  # read csv file 

n <- joinCountryData2Map(d, joinCode="NAME", nameJoinColumn="Countries")

colourPalette <- brewer.pal(6,"YlGn")
intervals<-c(-0.2,0,0.2,0.4,0.6,0.8,1)
intervals2<-c(0,0.2,0.4,0.6,0.8,1)
colourPalette2 <- brewer.pal(5,"YlGn")

mapParams <- mapCountryData(n, nameColumnToPlot="rhog", mapTitle="Full Sample correlation coefficient with Germany",
               xlim=c(-10, 40), ylim=c(35, 70),
               addLegend=FALSE,numCats=6,
               oceanCol="lightblue", missingCountryCol="white",catMethod=intervals,colourPalette=colourPalette)

do.call( addMapLegend, c(mapParams,horizontal=TRUE,legendWidth=1))


mapParams <- mapCountryData(n, nameColumnToPlot="rhous", mapTitle="Full Sample correlation coefficient with USA",
                            xlim=c(-10, 40), ylim=c(35, 70),
                            addLegend=FALSE,numCats=6,
                            oceanCol="lightblue", missingCountryCol="white",catMethod=intervals,colourPalette=colourPalette)

do.call( addMapLegend, c(mapParams,horizontal=TRUE,legendWidth=1))

mapParams <- mapCountryData(n, nameColumnToPlot="cig", mapTitle="Concordance indicator with Germany",
                            xlim=c(-10, 40), ylim=c(35, 70),
                            addLegend=FALSE,numCats=5,
                            oceanCol="lightblue", missingCountryCol="white",catMethod=intervals2,colourPalette=colourPalette2)

do.call( addMapLegend, c(mapParams,horizontal=TRUE,legendWidth=1))


mapParams <- mapCountryData(n, nameColumnToPlot="cius", mapTitle="Concordance indicator with USA",
                            xlim=c(-10, 40), ylim=c(35, 70),
                            addLegend=FALSE,numCats=5,
                            oceanCol="lightblue", missingCountryCol="white",catMethod=intervals2,colourPalette=colourPalette2)

do.call( addMapLegend, c(mapParams,horizontal=TRUE,legendWidth=1))