function [hl,hi]=plotmbbq(out,varargin)
if nargin==1
    xargin={};
    myrange=get(out.series,'range');
end

if nargin==2
    xargin={};
    if ~isempty(varargin{1}) 
        myrange=varargin{1};
    else 
        myrange=get(out.series,'range');
    end
end

if nargin==3
    if ~isempty(varargin{1}) 
        myrange=varargin{1};
    else 
        myrange=get(out.series,'range');
    end   

    if ~isempty(varargin{2}) 
        xargin=varargin{1};
    else 
        xargin=get(out.series,'range');
    end   
end


ts_peaks=tseries(out.peaks,out.series(out.peaks));
ts_troughs=tseries(out.troughs,out.series(out.troughs));
hl=plot(myrange,[out.series,ts_peaks,ts_troughs],xargin);
set(hl(1),'color','blue','linewidth',2,'linestyle','-');
set(hl(2),'linestyle','none','marker','^','markeredgecolor','green','markerfacecolor','green');
set(hl(3),'linestyle','none','marker','v','markeredgecolor','red','markerfacecolor','red');


peaks=out.peaks;
troughs=out.troughs;

if troughs(1)<peaks(1)
troughs(1)=[];
end

if peaks(end)>troughs(end)
    peaks(end)=[];
end

if length(peaks)~=length(troughs)
error('# of peaks does not equal # of troughs');
end
hi=zeros(length(peaks),2);
for i=1:length(peaks)
hi(i,1)=peaks(i);
hi(i,2)=troughs(i);
end

for i=1:length(peaks)
highlight(hi(i,:));
end

end
