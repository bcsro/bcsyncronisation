clear all;
close all;
clc;

load heatmap_oecd.mat;




dates={'Ireland',qq(1973,1);
       'UK',qq(1973,1);
       'Finland',qq(1995,1);
       'Sweden',qq(1995,1);
       'Denmark',qq(1973,1);
       'Netherlands',qq(1979,1);
       'France',qq(1979,1);
       'Luxembourg',qq(1979,1);
       'Austria',qq(1995,1);
       'Belgium',qq(1979,1);
       'Spain',qq(1986,1);
       'Italy',qq(1979,1);
       'Portugal',qq(1986,1);
       'Greece',qq(1981,1);
       'Latvia',qq(2004,2);
       'Estonia',qq(2004,2);
       'Lithuania',qq(2004,2);
       'Malta',qq(2004,2);
       'Cyprus',qq(2004,2);
       'Bulgaria',qq(2007,1);
       'Romania',qq(2007,1);
       'Czech_Republic',qq(2004,2);
       'Hungary',qq(2004,2);
       'Slovenia',qq(2004,2);
       'Slovakia',qq(2004,2)};
   
lc=length(dates);
rho=zeros(25,4);

for i=1:lc
x1=heatdb.Germany;
x1_range=get(x1,'range');
x2=heatdb.USA;
x2_range=get(x2,'range');
x3=heatdb.(dates{i,1});
x3_range=get(x3,'range');

temp=corr(double([x1{x1_range(1):dates{i,2}-1},x3{x3_range(1):dates{i,2}-1}]));
rho(i,1)=temp(1,2);
temp=corr(double([x1{dates{i,2}:qq(2014,3)},x3{dates{i,2}:qq(2014,3)}]));
rho(i,2)=temp(1,2);

temp=corr(double([x2{x2_range(1):dates{i,2}-1},x3{x3_range(1):dates{i,2}-1}]));
rho(i,3)=temp(1,2);
temp=corr(double([x2{dates{i,2}:qq(2014,3)},x3{dates{i,2}:qq(2014,3)}]));
rho(i,4)=temp(1,2);



end

myrep=report.new('Correlation split');

myrep.matrix('Correlation',rho,'rowNames',dates(:,1),...
                                           'colNames',{'Germany Pre EU','Germany Post EU',...
                                           'USA Pre EU','USA Post EU'});
                                       
hf=figure;

scatter(rho(:,1),rho(:,3));
title('Pre EU');
set(gca,'ylim',[-0.1,1],'xlim',[-0.1,1]);
xlabel Germany;
ylabel US;
hold on;
plot([-0.1,1],[-0.1,1]);

h = labelpoints (rho(:,1), rho(:,3), dates(:,1),'position','NE');

myrep.userfigure('',hf);


hf=figure;

scatter(rho(:,2),rho(:,4));
title('Post EU');
set(gca,'ylim',[-0.1,1],'xlim',[-0.1,1]);
xlabel Germany;
ylabel US;
hold on;
plot([-0.1,1],[-0.1,1]);

h = labelpoints (rho(:,2), rho(:,4), dates(:,1),'position','NE');

myrep.userfigure('',hf);

myrep.publish(['Report10_rho_scatter'  strrep(strrep(datestr(now),':','_'),' ','_') '.pdf']);