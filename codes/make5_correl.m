clear all;
close all;
clc;
load heatmap_oecd.mat;



% countries=fieldnames(heatdb);
countries={'Iceland','Ireland','UK','Switzerland','Germany','USA','Norway','Finland','Sweden','Denmark','Netherlands','France',...
                'Luxembourg','Austria','Belgium','Spain','Italy','Portugal','Greece',...
                'Latvia','Estonia','EU_28','Lithuania','Malta','Cyprus',...
                'Bulgaria','Romania','Czech_Republic','Hungary','Slovenia','Slovakia'};
            
lc=length(countries);
countries2=cell(1,lc);
for i=1:lc
countries2{i}=strrep(countries{i},'_',' ');
end


tstatrho=nan(lc,lc);
pvrho=zeros(lc,lc);
rho=ones(lc,lc);
stderr=nan(lc,lc);

for i=1:lc
    for j=i+1:lc
    if any(heatdb.(countries{i})-heatdb.(countries{j}))  
    [rho(i,j),stderr(i,j),tstatrho(i,j),pvrho(i,j)]=compute_rhomean(heatdb.(countries{i}),heatdb.(countries{j}));
    stderr(j,i)=stderr(i,j);
    tstatrho(j,i)=tstatrho(i,j);
    pvrho(j,i)=pvrho(i,j);
    rho(j,i)=rho(i,j);
    else
    display(['Countries ' countries{i} ' and ' countries{j} ' have the same S']);
    tstatrho(i,j)=NaN;
    tstatrho(j,i)=NaN;
    pvrho(i,j)=0;
    pvrho(j,i)=0;
    rho(i,j)=1;
    rho(j,i)=1;
    stderr(i,j)=0;
    stderr(j,i)=0;
    end
    end
end

simp_mat=nan(2*lc,2);
posG=find(ismember(countries,'Germany'));
posUS=find(ismember(countries,'USA'));
countries3=cell(1,2*lc);
for i=1:lc
simp_mat(2*i-1,1)=rho(posG,i);
simp_mat(2*i-1,2)=rho(posUS,i);

simp_mat(2*i,1)=stderr(posG,i);
simp_mat(2*i,2)=stderr(posUS,i);

countries3{2*i-1}=countries2{i};
countries3{2*i}='';
end

list_base={'USA','Germany'};
lags=-8:1:8;
rholeadlag=nan(length(list_base),lc,length(lags));
stdleadlag=nan(length(list_base),lc,length(lags));
pvleadlag=nan(length(list_base),lc,length(lags));
tstatleadlag=nan(length(list_base),lc,length(lags));
strlags=cell(length(lags),1);
for lag=1:length(lags)
    strlags{lag}=num2str(lags(lag));
end
for i=1:length(list_base)

    for j=1:lc
%         if strcmp(list_base{i},countries{j})
%             continue;
%         end
           for lag=1:length(lags)
               x1=heatdb.(list_base{i});
               x2=heatdb.(countries{j});
           [rholeadlag(i,j,lag),stdleadlag(i,j,lag),tstatleadlag(i,j,lag),pvleadlag(i,j,lag)]=...
               compute_rhomean(x1,x2{lags(lag)});    
           end

    end

end

ci=eye(lc);


for i=1:lc
    for j=i+1:lc
    ci(i,j)=concordanceindex(heatdb.(countries{i}),heatdb.(countries{j}));
    ci(j,i)=ci(i,j);
    end
end

% ci_sum=nan(lc,2);

ci_sum=ci(:,[posG,posUS]);

colormaxim=struct;
colormaxim.test='value == max(rowvalues)';
colormaxim.format='\color{red}';

myrep=report.new('Testing synchronisation');

myrep.matrix('Concordance index',ci,'rowNames',countries,...
                                           'colNames',countries);
                                       
myrep.matrix('Concordance index summary',ci_sum,'rowNames',countries,...
                                           'colNames',{'Germany','US'});                                       

myrep.matrix('Summary rho',simp_mat,'rowNames',countries3,...
                                           'colNames',{'Germany','US'});
                                       
                                       
myrep.matrix('rho',rho,'rowNames',countries,...
                                           'colNames',countries);
myrep.matrix('tstat for rho=0',tstatrho,'rowNames',countries,...
                                           'colNames',countries);
myrep.matrix('pvalue for rho=0',pvrho,'rowNames',countries,...
                                           'colNames',countries); 
                                       
hf=figure;

scatter(rho(:,posG),rho(:,posUS));
title('Correlation coefficient');
set(gca,'ylim',[-0.2,0.8],'xlim',[-0.2,0.8]);
xlabel Germany;
ylabel US;
hold on;
plot([-0.2,1],[-0.2,1]);

h = labelpoints (rho(:,posG),rho(:,posUS), countries2,'position','NE','FontSize',10);

myrep.userfigure('',hf);
                                       
                                       
                                       
for i=1:length(list_base)  
% x1=;
myrep.matrix(['rho lead lag against ' list_base{i} ' lags apply to long list of countries'], squeeze(rholeadlag(i,:,:)),'rowNames',countries,...
                                           'colNames',strlags,'condFormat=', colormaxim);
myrep.matrix(['tstat for rho=0 lead lag against ' list_base{i}],squeeze(tstatleadlag(i,:,:)),'rowNames',countries,...
                                           'colNames',strlags);
myrep.matrix(['pvalue for rho=0 lead lag against ' list_base{i}],squeeze(pvleadlag(i,:,:)),'rowNames',countries,...
                                           'colNames',strlags);                                         
end                                       

corrll=nan(2*lc,4);

for i=1:lc
    pos_max=find(squeeze(rholeadlag(1,i,:))==max(squeeze(rholeadlag(1,i,:))));
    
    corrll(2*i-1,1)=squeeze(rholeadlag(1,i,pos_max(1)));
    corrll(2*i-1,2)=lags(pos_max(1));
    corrll(2*i,1)=squeeze(stdleadlag(1,i,pos_max(1)));
    corrll(2*i,2)=squeeze(pvleadlag(1,i,pos_max(1)));
    
    pos_max=find(squeeze(rholeadlag(2,i,:))==max(squeeze(rholeadlag(2,i,:))));
    
    corrll(2*i-1,3)=squeeze(rholeadlag(2,i,pos_max(1)));
    corrll(2*i-1,4)=lags(pos_max(1));
    corrll(2*i,3)=squeeze(stdleadlag(2,i,pos_max(1)));
    corrll(2*i,4)=squeeze(pvleadlag(2,i,pos_max(1)));
end

myrep.matrix('Maximum rho',corrll,'long=',true,'format=','%.3f','rownames',countries3,'columnnames',{'Germany rho','Germany lag','US rho','US lag'});

myrep.publish(['Report5_Test_sync'  strrep(strrep(datestr(now),':','_'),' ','_') '.pdf'],...
    'tempDir=','Rep5tmp','cleanup=',false);
% 
% save rhos rho countries list_base ci;