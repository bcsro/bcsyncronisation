% Make the input data base for dating
clear all
close all

currentDir=cd;

cd('input');


inputDataT=readtable('all_gdp_data.txt'); %  Millions of national currency, chained volume estimates, national reference year, quarterly levels, seasonally adjusted
%inputDataT=readtable('Eurostat_GDP.txt');

gdp=struct;

dates=inputDataT{:,'dates'};
inputDataT.dates=[];
ld=length(dates);
datesiris=zeros(ld,1);

for i=1:ld
data=dates{i};    
datesiris(i)=qq(str2double(data(1:4)),str2double(data(end)));
end

lcountries=inputDataT.Properties.VariableNames;
llc=length(lcountries);

for i=1:llc
gdp.(lcountries{i})=tseries;
gdp.(lcountries{i})(datesiris)=100*log(inputDataT.(lcountries{i}));
gdp.(lcountries{i})=comment(gdp.(lcountries{i}),lcountries{i});
end


cd(currentDir);

%save gdp_Eurostat.mat gdp
save gdp_oecd.mat gdp
