clear all;
close all;
clc;

 load gdp_oecd.mat


gdp=rmfield(gdp,{'Poland','Croatia'}); % Remove countries with no dating
% gdp=rmfield(gdp,{ 'Belgium','Denmark','Estonia','Ireland',...
%     'Latvia','Luxembourg','Malta','Netherlands',...
%     'Austria','Slovenia','Slovakia','UK','Switzerland','Poland'});
countries=fieldnames(gdp);
lc=length(countries);

dating=struct;

for i=1:lc
try
    dating.(countries{i})=mbbq(gdp.(countries{i}),'thresh',5,'complete',0);
catch
display(['Could not date: ', countries{i} ,' index:', num2str(i)]);    
end

end

save dating_oecd.mat dating