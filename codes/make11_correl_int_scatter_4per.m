clear all;
close all;
clc

% load dating
load heatmap_oecd.mat;
% periods_end=[qq(1979,1),qq(2001,4),qq(2014,3)]; %v1 3 per
% periods_end=[qq(1979,1),qq(2001,4),qq(2008,2),qq(2014,3)]; %v1
% periods_end=[qq(1979,1),qq(1990,1),qq(2007,3),qq(2014,3)]; %v2
 periods_end=[qq(1979,1),qq(2001,4),qq(2008,4),qq(2014,3)]; %v3

countries=fieldnames(heatdb);
lc=length(countries);
lp=length(periods_end);
rho_var=nan(lp,lc,lc);

heatdb_range=dbrange(heatdb);

heatdb1=dbclip(heatdb,heatdb_range(1):periods_end(1));
heatdb_array=db2array(heatdb1);
rho_var(1,:,:)=corr(heatdb_array,'rows','pairwise');

for i=2:lp
heatdb1=dbclip(heatdb,periods_end(i-1)+1:periods_end(i));
heatdb_array=db2array(heatdb1);
rho_var(i,:,:)=corr(heatdb_array,'rows','pairwise');
end

myrep=report.new('Variable correlation');

myrep.matrix(strcat('Rho for period:',dat2str(heatdb_range(1)),'-',dat2str(periods_end(1))), squeeze(rho_var(1,:,:)),'rowNames',countries,...
                'colNames',countries);   

for i=2:lp

myrep.matrix(strcat('Rho for period:',dat2str(periods_end(i-1)+1),'-',dat2str(periods_end(i))), squeeze(rho_var(i,:,:)),'rowNames',countries,...
                'colNames',countries);    
end       

posG=find(ismember(countries,'Germany'));
posUS=find(ismember(countries,'USA'));

rhoall=nan(lc,lp*2);

for i=1:lp
rhoall(:,i)=squeeze(rho_var(i,:,posG));
rhoall(:,i+lp)=squeeze(rho_var(i,:,posUS));
end

countries1 ={'EU_28'
    'Belgium'
    'Bulgaria'
    'Czech_Republic'
    'Denmark'
    'Germany'
    'Estonia'
    'Ireland'
    'Greece'
    'Spain'
    'France'
    'Italy'
    'Cyprus'
    'Latvia'
    'Lithuania'
    'Luxembourg'};
countries2={ 
    'Hungary'
    'Malta'
    'Netherlands'
    'Austria'
    'Portugal'
    'Romania'
    'Slovenia'
    'Slovakia'
    'Finland'
    'Sweden'
    'UK'
    'EA_19'
    'Switzerland'
    'Iceland'
    'Norway'
    'USA'};

myrep.matrix('Rho agregated', rhoall(1:16,:),'rowNames',countries1,...
                'colNames',{['Germany:',char(dat2str(heatdb_range(1))),'-',char(dat2str(periods_end(1)))],...
                            ['Germany:',char(dat2str(periods_end(1)+1)),'-',char(dat2str(periods_end(2)))],...
                            ['Germany:',char(dat2str(periods_end(2)+1)),'-',char(dat2str(periods_end(3)))],...
                            ['Germany:',char(dat2str(periods_end(3)+1)),'-',char(dat2str(periods_end(4)))],...
                            ['USA:',char(dat2str(heatdb_range(1))),'-',char(dat2str(periods_end(1)))],...
                            ['USA:',char(dat2str(periods_end(1)+1)),'-',char(dat2str(periods_end(2)))],...
                            ['USA:',char(dat2str(periods_end(2)+1)),'-',char(dat2str(periods_end(3)))],...
                            ['USA:',char(dat2str(periods_end(3)+1)),'-',char(dat2str(periods_end(4)))]});
                        
myrep.matrix('Rho agregated', rhoall(17:32,:),'rowNames',countries2,...
                'colNames',{['Germany:',char(dat2str(heatdb_range(1))),'-',char(dat2str(periods_end(1)))],...
                            ['Germany:',char(dat2str(periods_end(1)+1)),'-',char(dat2str(periods_end(2)))],...
                            ['Germany:',char(dat2str(periods_end(2)+1)),'-',char(dat2str(periods_end(3)))],...
                            ['Germany:',char(dat2str(periods_end(3)+1)),'-',char(dat2str(periods_end(4)))],...
                            ['USA:',char(dat2str(heatdb_range(1))),'-',char(dat2str(periods_end(1)))],...
                            ['USA:',char(dat2str(periods_end(1)+1)),'-',char(dat2str(periods_end(2)))],...
                            ['USA:',char(dat2str(periods_end(2)+1)),'-',char(dat2str(periods_end(3)))],...
                            ['USA:',char(dat2str(periods_end(3)+1)),'-',char(dat2str(periods_end(4)))]});

                        
                        rhoall(6,:)=[]; % Bye-bye Germany
rhoall(31,:)=[]; % Bye-bye US
rhoall(1,:)=[]; % Bye-bye EU
rhoall(26,:)=[]; % Bye-bye EA

countries ={'Belgium'
    'Bulgaria'
    'Czech Republic'
    'Denmark'
    'Estonia'
    'Ireland'
    'Greece'
    'Spain'
    'France'
    'Italy'
    'Cyprus'
    'Latvia'
    'Lithuania'
    'Luxembourg'
    'Hungary'
    'Malta'
    'Netherlands'
    'Austria'
    'Portugal'
    'Romania'
    'Slovenia'
    'Slovakia'
    'Finland'
    'Sweden'
    'UK'
    'Switzerland'
    'Iceland'
    'Norway'};

%% Pre ERM period
hf1=figure;

scatter(rhoall(:,1),rhoall(:,5));
title(['Pre ERM period -',char(dat2str(heatdb_range(1))),'-',char(dat2str(periods_end(1)))],...
    'FontWeight','bold','FontSize',14);
set(gca,'ylim',[-0.2,1],'xlim',[-0.2,1]);
xlabel('{Correlation with the Germany}','FontWeight','bold','FontSize',14);
ylabel('{Correlation with the US}','FontWeight','bold','FontSize',14);
hold on;
plot([-0.2,1],[-0.2,1]);

h = labelpoints (rhoall(:,1), rhoall(:,5), countries(:),'position','NE');
%%
myrep.userfigure('',hf1);

%% ERM - EURO adoption
hf2=figure;

scatter(rhoall(:,2),rhoall(:,6));
title(['ERM - pre euro adoption -',char(dat2str(periods_end(1)+1)),'-',char(dat2str(periods_end(2)))],...
    'FontWeight','bold','FontSize',14);
set(gca,'ylim',[-0.2,1],'xlim',[-0.2,1]);
xlabel('{Correlation with the Germany}','FontWeight','bold','FontSize',14);
ylabel('{Correlation with the US}','FontWeight','bold','FontSize',14);
hold on;
plot([-0.2,1],[-0.2,1]);

h = labelpoints (rhoall(:,2), rhoall(:,6), countries(:),'position','NE');
%%
myrep.userfigure('',hf2);

%% EURO adoption - fall of Lehman
        hf3=figure;

scatter(rhoall(:,3),rhoall(:,7));
title(['Euro adoption - sharpest 2008 decline',char(dat2str(periods_end(2)+1)),'-',char(dat2str(periods_end(3)))],...
    'FontWeight','bold','FontSize',14);
set(gca,'ylim',[-0.3,1],'xlim',[-0.3,1]);
xlabel('{Correlation with the Germany}','FontWeight','bold','FontSize',14);
ylabel('{Correlation with the US}','FontWeight','bold','FontSize',14);
hold on;
plot([-0.3,1],[-0.3,1]);

h = labelpoints (rhoall(:,3), rhoall(:,7), countries(:),'position','NE');
%%
myrep.userfigure('',hf3);           

%% Post Lehman
hf4=figure;

scatter(rhoall(:,4),rhoall(:,8));
title(['Recovery -',char(dat2str(periods_end(3)+1)),'-',char(dat2str(periods_end(4)))],...
    'FontWeight','bold','FontSize',14);
set(gca,'ylim',[-0.2,1],'xlim',[-0.2,1]);
xlabel('{Correlation with the Germany}','FontWeight','bold','FontSize',14);
ylabel('{Correlation with the US}','FontWeight','bold','FontSize',14);
hold on;
plot([-0.2,1],[-0.2,1]);

h = labelpoints (rhoall(:,4), rhoall(:,8), countries(:),'position','NE');
%%
myrep.userfigure('',hf4); 
myrep.publish(['Report8_rho_var_'  strrep(strrep(datestr(now),':','_'),' ','_') '.pdf']);