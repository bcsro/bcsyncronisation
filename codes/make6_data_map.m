clear all;
close all;
clc;

load rhos.mat;

indG=find(ismember(countries,'Germany'));
indUS=find(ismember(countries,'USA'));

rhog=rho(:,indG);
rhous=rho(:,indUS);
cig=ci(:,indG);
cius=ci(:,indUS);

rhog(ismember(countries,'EU_28'))=[];
rhous(ismember(countries,'EU_28'))=[];
cig(ismember(countries,'EU_28'))=[];
cius(ismember(countries,'EU_28'))=[];
countries(ismember(countries,'EU_28'))=[];

rhog(ismember(countries,'EA_19'))=[];
rhous(ismember(countries,'EA_19'))=[];
cig(ismember(countries,'EA_19'))=[];
cius(ismember(countries,'EA_19'))=[];
countries(ismember(countries,'EA_19'))=[];


countries(ismember(countries,'Czech_Republic'))={'Czech Republic'};
countries(ismember(countries,'UK'))={'United Kingdom'};



tabel=table(countries,rhog,rhous, cig, cius,'VariableNames',{'Countries','rhog','rhous','cig','cius'});

cdir=cd;
cdir=[cdir '/Map'];

writetable(tabel,[cdir '/rhofullsample.txt']);

