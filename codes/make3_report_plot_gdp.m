clear all;
close all;
clc

% load gdp
load gdp_oecd

countries=fieldnames(gdp);
lc=length(countries);


% Make figures with all GDPs

% #1

hf1=figure('position',[680 268 1106 710],'paperorientation','landscape');
for i=1:12
subplot(3,4,i);
range_s=get(gdp.(countries{i}),'range');
a1=floor((range_s(end)-range_s(1))/2);
plot(gdp.(countries{i}),'datetick',range_s(1):a1:range_s(end),'dateformat','YYYY:P');
title(strrep(gdp.(countries{i}).comment,'_',' '));
grid on;
end
saveas(hf1,'Figure_A1.pdf');

hf2=figure('position',[680 268 1106 710],'paperorientation','landscape');
for i=13:24
subplot(3,4,i-12);
range_s=get(gdp.(countries{i}),'range');
a1=floor((range_s(end)-range_s(1))/2);
plot(gdp.(countries{i}),'datetick',range_s(1):a1:range_s(end),'dateformat','YYYY:P');
title(strrep(gdp.(countries{i}).comment,'_',' '));
grid on;
end
saveas(hf2,'Figure_A2.pdf');


hf3=figure('position',[680 268 1106 710],'paperorientation','landscape');
for i=25:lc
subplot(3,4,i-24);
range_s=get(gdp.(countries{i}),'range');
a1=floor((range_s(end)-range_s(1))/2);
plot(gdp.(countries{i}),'datetick',range_s(1):a1:range_s(end),'dateformat','YYYY:P');
title(strrep(gdp.(countries{i}).comment,'_',' '));
grid on;
end
saveas(hf3,'Figure_A3.pdf');


countries_heat={'Latvia','Estonia','Lithuania','Cyprus',...
                'Bulgaria','Romania','Czech_Republic','Hungary','Slovenia','Slovakia'};
hf4=figure('position',[680 268 1106 710],'paperorientation','landscape');
for i=1:length(countries_heat)
subplot(4,3,i);
range_s=get(gdp.(countries_heat{i}),'range');
a1=floor((range_s(end)-range_s(1))/2);
plot(gdp.(countries_heat{i}),'datetick',range_s(1):a1:range_s(end),'dateformat','YYYY:P');
title(strrep(gdp.(countries_heat{i}).comment,'_',' '));
grid on;
end
saveas(hf4,'Figurerev.emf');

return

myrep=report.new('GDP data');

for i=1:lc

% if mod(i,2)==1
% myrep.align('',2,2);
% end

myrep.figure('');
myrep.graph(gdp.(countries{i}).comment);
myrep.series('',gdp.(countries{i}));

end
myrep.publish(['Report3_GDP_OECD_data_'  strrep(strrep(datestr(now),':','_'),' ','_') '.pdf']);