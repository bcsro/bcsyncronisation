clear all;
close all;
clc;

%% Load the data
[data,text]=xlsread('date.xlsx','db_q');

data_PCA=struct;


ld=length(text(:,1))-1;
datesiris=zeros(ld,1);

for i=1:ld
time=text{i+1,1};    
datesiris(i)=qq(str2double(time(1:4)),str2double(time(end)));
end


list_codes={'SI','SK','RO','MT','PL','BG','LT','LV','HU','EE','HR','CY','CZ','EU'};
list_countries={'Slovenia','Slovakia','Romania','Malta','Poland','Bulgaria','Lithuania','Latvia','Hungary','Estonia','Croatia','Cyprus','Czech_Republic','EU_28'};
for i=1:length(data)
name_indicator=text{1,i+1};
pos=strfind(name_indicator,'_');
if isempty(pos)
    continue
end
countrycode=name_indicator(pos(end)+1:end);
countryname=list_countries(strcmp(countrycode,list_codes));
indicator=name_indicator(1:pos(end)-1);

% Exclude 
% if strcmp(indicator,'M')
% continue
% end
data_PCA.(countryname{1}).(indicator)=tseries(datesiris,data(:,i));

if strcmp(indicator,'X')||strcmp(indicator,'M')  % if import or export seasonally adjust
data_PCA.(countryname{1}).(indicator)=x12(data_PCA.(countryname{1}).(indicator));
end

end

% Exclude empty data
data_PCA.Poland=rmfield(data_PCA.Poland,'CA_RET');

data_PCA_level=data_PCA;

%% Compute the PCA data in diference to stationarise it
countrylist=fieldnames(data_PCA);
data_PCA_delta=struct; % Define the difference data base

for i=1:length(countrylist)
db=data_PCA.(countrylist{i});    
indicators=fieldnames(db);

    for j=1:length(indicators)
        
    if strcmp(indicators{j},'ESI')|| strcmp(indicators{j},'I') % DO first difference
    data_PCA_delta.(countrylist{i}).(indicators{j})=diff(db.((indicators{j})));
    else                                                       % DO delta log    
    data_PCA_delta.(countrylist{i}).(indicators{j})=diff(100*log(db.((indicators{j}))));
    end
    
    end
    data_PCA_delta.(countrylist{i})=dbclip(data_PCA_delta.(countrylist{i}),qq(2000,1):qq(2014,1));
end

% return
%% Compute the PCA
PCA_estim=struct;
% For each country
for i=1:length(countrylist)
db=data_PCA_delta.(countrylist{i});
rangedb=dbrange(db);
[coeff,score,latent,tsquared,explained,mu] = pca(double(db2array(db)),'NumComponents',1);
if isempty(explained)
    i
end
PCA_estim.(countrylist{i}).score_delta=tseries(rangedb,score);
PCA_estim.(countrylist{i}).explained_delta=explained(1);
PCA_estim.(countrylist{i}).score_level=tseries;
range_PCA=get(PCA_estim.(countrylist{i}).score_delta,'range');
PCA_estim.(countrylist{i}).score_level(range_PCA(1)-1)=100;
PCA_estim.(countrylist{i}).score_level(range_PCA)=100+cumsum(PCA_estim.(countrylist{i}).score_delta);
end

PCA_estim=rmfield(PCA_estim,'Poland');
PCA_estim=rmfield(PCA_estim,'Croatia');

save PCA_estim PCA_estim data_PCA_level;